<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BrewProject</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
               
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Holidays

                    





                </div>

                <div class="links">
                    <form action="search" method="post">
                        <input type="date" name="startDate" >
                        <input type="date" name="endDate" >
                        <select name="city">
                            <option value="2344116"> İstanbul </option>
                            <option value="2343732"> Ankara </option>
                            <option value="2344117"> İzmir </option>
                        </select>
                        <input type="submit" value="Search">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                   
                </div>
                <br>
                <div style="color:black ">
                
              

                @if (count($bodyArray) != 0) 
                <table>
                    <tr>
                        <td>
                        <b>Holiday Name </b>
                        </td>
                        <td>
                        <b>Holiday Date</b>
                        </td>
                        <td>
                        <b>Holiday Day</b>
                        </td>
                        <td>
                        <b>Holiday Weather</b>
                        </td>
                        <td>
                        <b>Min Temp</b>
                        </td>
                        <td>
                        <b>Max Temp</b>
                        </td>
                    </tr>
                    @foreach($bodyArray as $holidays) 
                        @php
                            $startDate = Carbon\Carbon::createFromFormat('Y-m-d', $holidays->start)->format('Y-m-d');
                            $endDate = Carbon\Carbon::createFromFormat('Y-m-d', $holidays->end)->format('Y-m-d');
                        @endphp

                        @for($i = strtotime($startDate); $i <= strtotime($endDate); $i = strtotime("+1 day", $i))
                            @php
                                $day = explode('-', date("Y-m-d", $i));
                                $day = $day[2];
                                $date = date("d-m-Y", $i);
                            @endphp

                            <tr>
                                <td>
                                    {{ $holidays->name }}
                                </td>
                                <td>
                                    {{ $date }}
                                </td>
                                <td>
                                    {{ Carbon\Carbon::createFromFormat('d-m-Y', $date)->englishDayOfWeek }}
                                </td>
                                <td>
                                    @php echo $holidays->weatherName[$day]; @endphp
                                </td>
                                <td>
                                    @php echo $holidays->min[$day]; @endphp
                                </td>
                                <td>
                                    @php echo $holidays->max[$day]; @endphp
                                </td>
                            </tr>
                        @endfor

                    @endforeach
                </table>
                @else
                    Holiday Not Found
                @endif

                </div>
            </div>
        </div>
    </body>
</html>
