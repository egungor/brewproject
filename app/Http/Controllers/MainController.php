<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use GuzzleHttp\Client;
use Carbon\Carbon;

class MainController extends Controller
{
    
	public function show()
	{
		return view('welcome');
	}

	public function search(Request $request)
	{
		//get city
		$city = $request->city;

		//get dates
		$sDate = $request->startDate;
		$eDate   = $request->endDate;

		//get years
		$startDate = explode('-',$request->startDate);
		$endDate   = explode('-',$request->endDate);
		$startYear = $startDate[0];
		$endYear = $endDate[0];

		$client = new Client;
		$bodyArray = array();

		//get holidays from api
		for ($i = $startYear; $i <= $endYear; $i++) {

			$holidaysApi = $client->request('GET', 'https://calendarific.com/api/v2/holidays?&api_key=435604627ecd6530dfb8f43d74b83a18e5636aee&country=TR&type=national&year='.$i);
			$holidaysResponse = $holidaysApi->getBody(); 
			$holidaysResponse = json_decode($holidaysResponse);


			$bodyArray = array_merge($bodyArray, $holidaysResponse->response->holidays);

		}

		

			//filtering by input dates
			$bodyArray = array_filter( $bodyArray, function($var) use ($sDate, $eDate) {
	    		$time = $var->date->iso;
	    		return $time <= $eDate && $time >= $sDate;
	 		});

	 	if (count($bodyArray) != 0 ) { 

			// grouping consecutive days
	 		$group = array();
	        $i=0;
	        $j=1;
	        $diff=86400;
	        $groupHoliday = $diff;
	        $consecutive=0;
	        $group[$consecutive]['start'] = $bodyArray[$i]->date->iso;
	        $group[$consecutive]['end'] = $bodyArray[$i]->date->iso;
	        $group[$consecutive]['name'] = $bodyArray[$i]->name;

	        while($j<count($bodyArray)){

	            if(strtotime($bodyArray[$j]->date->iso)-strtotime($bodyArray[$i]->date->iso) == $groupHoliday){

	                $group[$consecutive]['end'] = $bodyArray[$j]->date->iso;
	                $group[$consecutive]['name'] = $bodyArray[$i]->name;
	                $j++;
	                $groupHoliday+=$diff;

	            } else {

	                $i=$j;
	                $j++;
	                $consecutive++;
	                $group[$consecutive]['start'] = $bodyArray[$i]->date->iso;
	                $group[$consecutive]['end'] = $bodyArray[$i]->date->iso;
	                $group[$consecutive]['name'] = $bodyArray[$i]->name;
	                $groupHoliday = $diff;

	            }

	        }

	       	$consecutiveGroup= json_decode(json_encode($group), FALSE);

	       	//finding long-weekends
	        foreach ($consecutiveGroup as $value) {


	        	
	        	if ($value->start == $value->end) {

	        		if ( Carbon::createFromFormat('Y-m-d', $value->start)->isMonday()) { 

	        			$value->start = Carbon::createFromFormat('Y-m-d', $value->start)->subDays(2)->format('Y-m-d');
	        			$value->startDayName = Carbon::createFromFormat('Y-m-d', $value->start)->englishDayOfWeek;
	        			$value->endDayName = Carbon::createFromFormat('Y-m-d', $value->end)->englishDayOfWeek;

	        		} else if ( Carbon::createFromFormat('Y-m-d', $value->start)->isFriday()) {

	        			$value->end = Carbon::createFromFormat('Y-m-d', $value->start)->addDays(2)->format('Y-m-d');
	        			$value->startDayName = Carbon::createFromFormat('Y-m-d', $value->start)->englishDayOfWeek;
	        			$value->endDayName = Carbon::createFromFormat('Y-m-d', $value->end)->englishDayOfWeek;

	        		} else {

	        			$value->startDayName = Carbon::createFromFormat('Y-m-d', $value->start)->englishDayOfWeek;
	        			$value->endDayName = Carbon::createFromFormat('Y-m-d', $value->end)->englishDayOfWeek;
	        		}

	        	} else {

	        		if (Carbon::createFromFormat('Y-m-d', $value->start)->isWeekday() && Carbon::createFromFormat('Y-m-d', $value->end)->isWeekday() && !(Carbon::createFromFormat('Y-m-d', $value->start)->isFriday()) && !(Carbon::createFromFormat('Y-m-d', $value->end)->isMonday()) ) {

	        			$value->start = Carbon::createFromFormat('Y-m-d', $value->start)->startOfWeek()->subDays(2)->format('Y-m-d');
	        			$value->end = Carbon::createFromFormat('Y-m-d', $value->end)->endOfWeek()->format('Y-m-d'); 
	        			$value->startDayName = Carbon::createFromFormat('Y-m-d', $value->start)->englishDayOfWeek;
	        			$value->endDayName = Carbon::createFromFormat('Y-m-d', $value->end)->englishDayOfWeek;

	        		} else if (Carbon::createFromFormat('Y-m-d', $value->start)->isSunday() && Carbon::createFromFormat('Y-m-d', $value->end)->isWeekday()) {

	        			$value->start = Carbon::createFromFormat('Y-m-d', $value->start)->subDays(1)->format('Y-m-d'); 
	        			$value->startDayName = Carbon::createFromFormat('Y-m-d', $value->start)->englishDayOfWeek;
	        			$value->endDayName = Carbon::createFromFormat('Y-m-d', $value->end)->englishDayOfWeek;
	        			
	        		} else if (Carbon::createFromFormat('Y-m-d', $value->start)->isWeekday() && Carbon::createFromFormat('Y-m-d', $value->end)->isSaturday()) {

	        			$value->end = Carbon::createFromFormat('Y-m-d', $value->end)->addDays(1)->format('Y-m-d');
	        			$value->startDayName = Carbon::createFromFormat('Y-m-d', $value->start)->englishDayOfWeek;
	        			$value->endDayName = Carbon::createFromFormat('Y-m-d', $value->end)->englishDayOfWeek;
	        		}

	        	}


			}

	 
			
	 		
	 		//get weather forecast from api
	 		foreach ($consecutiveGroup as $holidays) {
	 			
	 			$client = new Client;

	 			$startDate = Carbon::createFromFormat('Y-m-d', $holidays->start)->format('Y/m/d');
	 			$endDate = Carbon::createFromFormat('Y-m-d', $holidays->end)->format('Y/m/d');

	 			
	 			for ($i = strtotime($startDate); $i <= strtotime($endDate); $i = strtotime("+1 day", $i)) {

					$weatherApi = $client->request('GET', 'https://www.metaweather.com/api/location/'.$city.'/'.date("Y/m/d", $i).'/');
					$weatherResponse = $weatherApi->getBody(); 
					$weatherResponse = json_decode($weatherResponse);

					$day = explode('/', date("Y/m/d", $i));
					$day = $day[2];
					if(count($weatherResponse) == 0) {
						$holidays->weatherName[$day] = null;
						$holidays->min[$day] = null;
						$holidays->max[$day]= null;
					} else {
						$holidays->weatherName[$day] = $weatherResponse[0]->weather_state_name;
						$holidays->min[$day] = $weatherResponse[0]->min_temp;
						$holidays->max[$day] = $weatherResponse[0]->max_temp;
					}

					$j++;
				}
				
	 		}
	 		
	 		$bodyArray = $consecutiveGroup;

	 	} else {

	 		$bodyArray = [];
	 	}

 		
 
 		return view('welcome', compact('endYear','bodyArray', 'body', 'startYear', 'weatherResponse', 'sDate', 'eDate'));
	}

	

	

}
